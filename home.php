<?php 
get_header(); 
?>

<div id="featured" class="col-xs-12">
	
	<?php 
	global $post;

	$args = array( 'numberposts' => 1, 'category_name' => 'slides' );

	$posts = get_posts( $args );

	foreach( $posts as $post ): setup_postdata($post); 
		// check if the post has a Post Thumbnail assigned to it.
		if ( has_post_thumbnail() ) {
		?>
			<a href="<?=the_permalink();?>"><?=the_post_thumbnail();?></a>
		<?php
		} 
		?>
		<div class="caption col-xs-12">
			<a href="<?=the_permalink();?>"><?=the_title()?></a>
		</div><!-- /caption -->
	<?php endforeach; ?>
</div><!-- /featured -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>
