<?php
if ( is_front_page() ) {
	$sidebarClassAllExceptHome = "";
	$calloutClassHome = "col-sm-4";
}
else {
	$sidebarClassAllExceptHome = "col-sm-3";
	$calloutClassHome = "";
}
?>

<div id="secondary" class="col-xs-12 <?=$sidebarClassAllExceptHome?>">
<?php 
global $post;

$args = array( 'numberposts' => 3, 'category_name' => 'features' );

$posts = get_posts( $args );

foreach( $posts as $post ): setup_postdata($post); 
	?>
	<div class="callout <?=$calloutClassHome?> col-xs-12">
		<div class="col-xs-12">
		<a href="<?=the_permalink();?>"><?= the_post_thumbnail();?></a>
		<div class="caption  col-xs-12">
				<a href="<?=the_permalink();?>"><?=the_title();?></a>	
		</div><!-- /caption -->
		</div>
	</div><!-- /callout -->	
	<?php
endforeach;
?>
	

</div><!-- /secondary -->