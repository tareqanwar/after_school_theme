			</div>
			<!-- /content -->

			<!-- footer -->
			<footer class="main col-xs-12">
		
				<nav id="footer">
				
					<?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?>
				
				</nav>

				<p class="copyright">Copyright &copy; <?php echo date('Y'); ?> <?php echo  bloginfo('name');?> </p>
			</footer>
		
		</div><!-- /container -->

		<?php wp_footer(); ?>

	</body>
</html>
