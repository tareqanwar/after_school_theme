<?php
/*------------------------------------*\
	Functions
\*------------------------------------*/

// After School menus
function register_after_school_menus() {
register_nav_menus(
    array(
    'main-menu' => __( 'Main Menu' ),
    'util-menu' => __( 'Util Menu' ),
    'footer-menu' => __( 'Footer Menu' )
    )
);
}
add_action( 'init', 'register_after_school_menus' );

// Load styles & scripts
function register_after_school_scripts() {
    wp_enqueue_style( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css', array(), 20141119 );
    wp_enqueue_style( 'theme-style', get_stylesheet_directory_uri() . '/style.css', array(), 20141119 );
    // all scripts
    wp_enqueue_script( 'bootstrap', 'https//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js', array('jquery'), '20120206', true );
    wp_enqueue_script( 'theme-script', get_template_directory_uri() . '/js/script.js', array('jquery'), '20120206', true );
}
add_action( 'wp_enqueue_scripts', 'register_after_school_scripts' );

//enable featured image support
add_theme_support( 'post-thumbnails' ); 

//add body class
function after_school_body_class( $classes ) {
 
    if ( is_front_page( ) ) {
        $classes[] = 'home';
    }
    else {
        $classes[] = 'page-template-default';
    }
     
    return $classes;
     
}
add_filter( 'body_class','after_school_body_class' );